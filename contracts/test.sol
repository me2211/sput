pragma solidity >= "0.8.21";

contract test {
  function getTimestamp () public view returns (uint) {
    return block.timestamp;
  }
}
