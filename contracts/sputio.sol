pragma solidity >= "0.8.20";
import "./sput.sol";
// TODO - import isHandshake interface

contract sputio {
  address owner;
  struct spat {
    address creator;
    address sput;
  }
  address[] public sputs;
  mapping(address => spat) spats;
  constructor () {
    owner = msg.sender;
  }
  function _sput (string memory url, string memory counter, uint[] memory _timestamps, uint[] memory views, bool up, address _gitarg) public returns (address) {
    //constructor (string memory url, string memory counter, uint[] memory _timestamps, uint[] memory _views, bool up, address _gitarg) {
    sput sput_ = new sput(url, counter, _timestamps, views, up, _gitarg);
    // TODO - inverse?
    if(!up) {
      spats[msg.sender] = spat(msg.sender, address(sput_));
      return address(this);
    }
    sputs.push(address(sput_));
    return address(sput_);
  }
  function puts () public returns (address[] memory) {
    return sputs;
  }
  function _puts(address _spat) public returns (address) {
    spat memory spat_ = spats[_spat];
    require(spats[msg.sender].creator == msg.sender, "redundancy");
    return spat_.sput;
  }
}
