// SPDX-License-Identifier: BUSL-1.1
pragma solidity >= "0.8.20";
//import "./erc20.sol";
import "./gitarg.sol";

contract sput {

  uint[] timestamps;
  uint[] views;
  //mapping(viewcounts => timestamps) deadlines;
  struct deadline {
    uint timestamp;
    bool success;
  }
  mapping(uint => deadline) deadlines;
  struct spot {
    address owner;
    uint value;
  }
  mapping(uint => spot[]) placements;
  address owner;
  uint public total;
  bool down;

  address arg;
  gitarg Gitarg;

  string url;
  string counter;

  modifier timer (uint count) {
    deadline memory line = deadlines[count]; 
    require(line.timestamp > block.timestamp, "timed");
    _;
  }
  modifier own () {
    require(msg.sender == owner, "owned");
    _;
  }

  constructor (string memory _url, string memory _counter, uint[] memory _timestamps, uint[] memory _views, bool up, address _gitarg) {
    url = _url;
    counter = _counter;
    owner = msg.sender;
    timestamps = _timestamps;
    views = _views;
    down = !up;
    for (uint i = 0; i < views.length; i++) {
      deadlines[views[i]] = deadline(timestamps[i], false);
    }
    arg = _gitarg;
    Gitarg = gitarg(_gitarg);
  }
  function call () public own {
    for (uint i = 0; i < views.length; i++) {
      require(deadlines[views[i]].timestamp < block.timestamp, "undead");
    }
    payable(owner).transfer(address(this).balance);
  }
  function checkin (uint count) public timer(count) own {
    //storage
    deadline storage line = deadlines[count];
    line.success = true;
  }
  function check (uint count) public view returns (bool) {
    deadline memory line = deadlines[count];
    return line.success;
  }
  // the number of views has to equal the number of in bids minimum before payout for safety
  function placement (uint count, bool git) public payable {
    // TODO - review git bool allows payment in git but needs limiter
    total += msg.value;
    spot memory spot_ = spot(msg.sender, msg.value);
    //placements[count];
    placements[count].push(spot_);
  }
  function checkout (uint count) public payable {
    // TODO - git uses gitarg token as payout
    // TODO - review reward for first claims in mine and send shitty kill version back in time? 
    require(Gitarg.balanceOf(msg.sender) >= count, "court"); // ticket, m'lady
    uint round = 0; // 0 init anyway?
    spot[] memory spot_ = placements[count];
    deadline memory line = deadlines[count];
    require(line.success == true, "fail");
    
    spot memory _spot;
    for (uint i = 0; i < spot_.length; i++) {
      if (spot_[i].owner == msg.sender) {
        _spot = spot_[i];
        break;
      }
      round += spot_[i].value;
      // put here for equalality
    }
    uint cut = _spot.value / round;
    uint trickle = _spot.value / total;

    if (down) {
      Gitarg.transferFrom(owner, msg.sender, Gitarg.balanceOf(owner) / trickle);
      // 
      payable(msg.sender).transfer(cut * round);
    } else {
      Gitarg.transferFrom(owner, msg.sender, Gitarg.allowance(owner, msg.sender) / cut);
      //
      payable(msg.sender).transfer(trickle * round);
    }
    // the cut and the trickle added togother favor lower numbers and initial investment, passing advertisement standards
    // this avoids the human trafficking issue of buying views directly and the pyramid scheme issue to allow traffic direction
    // returning the trickle and cut as seperate tokens allows for base token acrew.
    // cut to integration

  }
}

