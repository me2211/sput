const sput = artifacts.require('sput')
const gitarg = artifacts.require('gitarg')

const test = artifacts.require('test')

module.exports = async function(_deployer, network, accounts) {
  // Use deployer to state migration tasks.
  const arg = await gitarg.deployed()
  const tester = await _deployer.deploy(test)
  //await arg.getTimestamp() // or create test interface //stub or driver?
  const timestamp = await tester.getTimestamp()
  console.log({ timestamp })
  const timestamps = [timestamp+ 10, timestamp + 30, timestamp + 100, timestamp + 120, timestamp + 500]
  const views = [100, 400, 1000, 10000, 100000]
  //constructor (uint[] memory _timestamps, uint[] memory _views, bool up, address _gitarg) {
  await _deployer.deploy(sput, "https://youtube.com/gitarg", "https://youtube.com/gitarg", timestamps, views, false, arg.address)
};
